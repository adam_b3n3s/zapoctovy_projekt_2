// This file is having the methods and definition of linked list
// It is used in fibonacci heap

using System;
using System.Collections.Generic;
using static System.Console;
using node;
namespace linked_list
{
    public class linked_list<T>
    {
        public node<T> head;
        public node<T> tail;
        public node<T> parent;
        public node<T> childern;
        public int number_of_elements = 0;

        // Linked list is able to make instances two ways
        // 1) If you want to put in arguments as Array you can
        // 2) If you want to put in arguments as List you can
        public linked_list(params T[] args) // O(n)
        {
            // When creating linked list all elements are put with add_last to the back of it one by one
            foreach (var item in args)
            {
                add_last(item);
            }
        }
        public linked_list(List<T> numbers) // O(n)
        {
            // When creating linked list all elements are put with add_last to the back of it one by one
            foreach (var item in numbers)
            {
                add_last(item);
            }
        }

        public void merge(linked_list<T> list2) // O(1)
        // Void merge is used to put together two lists
        {
            // Connect the end of the first list to start of the first one
            this.tail.next = list2.head;
            list2.head.previous = this.tail;
            this.tail = list2.tail;
            this.head.previous = this.tail;
            this.tail.next = this.head;

            // We must consider that number of elements must be added together
            this.number_of_elements = this.number_of_elements + list2.number_of_elements;
        }

        public void delete_node(node<T> node) // O(1)
        // Delete given node from the list
        {
            // When deleting node from list that has just one node
            if (head == tail && head == node)
            {
                head = null;
                tail = null;
                this.number_of_elements = this.number_of_elements - 1;
                return;
            }

            // Deleting head
            if (head == node)
            {
                // When deleting head, must change the head to next node
                head = head.next;
                head.previous = tail;
                tail.next = head;
                this.number_of_elements = this.number_of_elements - 1;
                return;
            }

            // Deleting tail
            if (tail == node)
            {
                // When deleting tail, must change the tail to previous node
                tail = tail.previous;
                tail.next = head;
                head.previous = tail;
                this.number_of_elements = this.number_of_elements - 1;
                return;
            }

            // If node is something else it is easy
            node.previous.next = node.next;
            node.next.previous = node.previous;
            this.number_of_elements = this.number_of_elements - 1;
        }

        public void add_first(node<T> new_node) // O(1)
        // Adds node as a first one
        {
            if (head == null)
            {
                // If the head doesnt exists => list is empty => put it as only node
                head = new_node;
                tail = new_node;
                new_node.next = new_node;
                new_node.previous = new_node;
            }
            else
            {
                // Otherwise we can just add it to the front
                new_node.next = head;
                new_node.previous = tail;
                head.previous = new_node;
                tail.next = new_node;
                head = new_node;
            }
            this.number_of_elements = this.number_of_elements + 1; // And increase the number of elements
        }

        public void add_first(T value) // O(1)
        // Same as add_first just putting the value and node (just for friendly use)
        {
            // Create node with this value
            node<T> new_node = new node<T>(value);

            if (head == null)
            {
                // If the head doesnt exists => list is empty => put it as only node
                head = new_node;
                tail = new_node;
                new_node.next = new_node;
                new_node.previous = new_node;
            }
            else
            {
                // Otherwise we can just add it to the front
                new_node.next = head;
                new_node.previous = tail;
                head.previous = new_node;
                tail.next = new_node;
                head = new_node;
            }
            this.number_of_elements = this.number_of_elements + 1;
        }

        public void add_last(node<T> new_node) // O(1)
        // Adds node to the end of the list
        {
            if (tail == null)
            {
                // If the tail doesnt exists => list is empty => put it as only node
                head = new_node;
                tail = new_node;
                new_node.next = new_node;
                new_node.previous = new_node;
            }
            else
            {
                // Otherwise we can just add it to the back
                new_node.next = head;
                new_node.previous = tail;
                head.previous = new_node;
                tail.next = new_node;
                tail = new_node;
            }
            this.number_of_elements = this.number_of_elements + 1;
        }

        public void add_last(T value) // O(1)
        // Adds value to the end of the list
        {
            // Create node with this value
            node<T> new_node = new node<T>(value);

            if (tail == null)
            {
                // If the tail doesnt exists => list is empty => put it as only node
                head = new_node;
                tail = new_node;
                new_node.next = new_node;
                new_node.previous = new_node;
            }
            else
            {
                // Otherwise we can just add it to the back
                new_node.next = head;
                new_node.previous = tail;
                head.previous = new_node;
                tail.next = new_node;
                tail = new_node;
            }
            this.number_of_elements = this.number_of_elements + 1;
        }
        public void print_list() // O(n)
        // Prints the list with spaces between elements
        {
            if (head == null)
            {
                // If list is empty print empty
                WriteLine("Empty");
                return;
            }

            // Otherwise print all elements
            node<T> current = head;

            while (current != tail)
            {
                // While we are not at the end print the value and space
                Write(current.value + " ");
                current = current.next;
            }
            Write(current.value);
            WriteLine(); // And print new line
        }
        public string return_list() // O(n)
        // Returns the string that would be printed by print_list
        {
            string q = "";
            if (head == null)
            {
                // If list is empty return "empty"
                return "empty";
            }

            // Otherwise return all elements
            node<T> current = head;

            while (current != tail)
            {
                // While we are not at the end return the value and space
                q += current.value + " ";
                current = current.next;
            }
            q += current.value;
            return q;
        }
    }

}