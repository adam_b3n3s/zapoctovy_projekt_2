﻿// This file is used to be main file of whole project

using System;
using System.Collections.Generic;
using static System.Console;
using node;
using linked_list;
using fibonacci_heap;
using unit_tests;

class main_file
{
    static void Main(string[] args)
    // This is main void for the whole project
    {
        WriteLine("Running tests:");
        fibonacci<double> fib1 = new fibonacci<double>(-2, 4, 0, 5, 7, 8, 9, 11, 34, 56, 78, 32, 12, 45, 23, 67, 98, 87, -1);

        tests<double> test_instance = new tests<double>();

        fib1.extract_min();

        fib1.extract_min();

        fib1.decrease(fib1.fibonacci_heap_main.head.son.head.next.next.son.head.next, -3);

        fib1.decrease(fib1.fibonacci_heap_main.head.son.head.next.next.son.head, -2);

        string answer1 = "#elements in heap: 17\n#elements in root: 5\nRoot: 0 87 -3 -2 8\n0: 4 5 12\n0: 5: 7\n0: 12: 32 56 23\n0: 12: 56: 78\n0: 12: 23: 45 67\n0: 12: 23: 67: 98\n-3: 34\n";

        WriteLine("Test 1: "+test_instance.test(fib1, answer1));
        
        fibonacci<double> fib2 = new fibonacci<double>(2);

        fib2.extract_min();

        string answer2 = "#elements in heap: 0\n#elements in root: 0\nempty\n";
        
        WriteLine("Test 2: "+test_instance.test(fib2, answer2));
        
        fibonacci<double> fib3 = new fibonacci<double>(1, 2, 3, 4);

        fib3.extract_min();

        string answer3 = "#elements in heap: 3\n#elements in root: 2\nRoot: 4 2\n2: 3\n";
        
        WriteLine("Test 3: "+test_instance.test(fib3, answer3));
        
        fib3.delete(fib3.fibonacci_heap_main.head);
        
        answer3 = "#elements in heap: 2\n#elements in root: 1\nRoot: 2\n2: 3\n";
        
        WriteLine("Test 4: "+test_instance.test(fib3, answer3));
        
        fibonacci<double> fib5 = fib1;

        fib5.extract_min();
        
        fib5.delete(fib1.fibonacci_heap_main.head.son.head.next.next.son.head);
        
        fib5.delete(fib1.fibonacci_heap_main.head.son.head.next.next.son.head);
        
        string answer5 = "#elements in heap: 14\n#elements in root: 5\nRoot: 0 8 -2 12 34\n0: 4 5\n0: 5: 7\n-2: 87\n12: 23\n12: 23: 45 67\n12: 23: 67: 98\n34: 78\n";
        
        WriteLine("Test 5: "+test_instance.test(fib5, answer5));
        
        while(fib5.number_of_elements > 0)
        {
            fib5.delete(fib5.minimum);
        }
        
        string answer6 = "#elements in heap: 0\n#elements in root: 0\nempty\n";
        
        WriteLine("Test 6: "+test_instance.test(fib5, answer6));
    }
}