\babel@toc {czech}{}\relax 
\thispagestyle {toc}
\contentsline {chapter}{\numberline {1}Úvod a cíl práce}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Cíle}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Srovnání Fibonacciho haldy s binární haldou (amortizovaně)}{1}{section.1.2}%
\contentsline {chapter}{\numberline {2}Průběh tvoření}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Na co jsem si dával pozor}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Unit testy}{2}{section.2.2}%
\contentsline {section}{\numberline {2.3}Generický typ}{2}{section.2.3}%
\contentsline {section}{\numberline {2.4}Make list a make heap}{3}{section.2.4}%
\contentsline {chapter}{\numberline {3}Složitosti operací}{4}{chapter.3}%
\contentsline {section}{\numberline {3.1}Double circular linked list}{4}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Make list}{4}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Merge}{4}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Delete node}{4}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Add first}{4}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Add last}{4}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Print list}{4}{subsection.3.1.6}%
\contentsline {section}{\numberline {3.2}Fibonacciho halda}{5}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Lemma, $1+\displaystyle \DOTSB \sum@ \slimits@ F_i = F_{n+2}$}{5}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Lemma, rychlost růstu Fibonacciho čísel}{5}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Definice (podstromu)}{6}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Věta}{6}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Amortizovaná časová složitost Fibonacciho haldy}{7}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Definice (potenciálu)}{7}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Definice (amortizované složitosti)}{8}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Náš potenciál}{8}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Make heap}{8}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Insert}{8}{subsection.3.3.5}%
\contentsline {subsection}{\numberline {3.3.6}Min value}{8}{subsection.3.3.6}%
\contentsline {subsection}{\numberline {3.3.7}Merge heap}{9}{subsection.3.3.7}%
\contentsline {subsection}{\numberline {3.3.8}Print heap}{9}{subsection.3.3.8}%
\contentsline {subsection}{\numberline {3.3.9}Lemma, opakovaně extract min}{9}{subsection.3.3.9}%
\contentsline {subsection}{\numberline {3.3.10}Lemma, opakovaně operace cut}{10}{subsection.3.3.10}%
\contentsline {subsection}{\numberline {3.3.11}Jak z operace cut plyne složitost decrease a delete}{10}{subsection.3.3.11}%
\contentsline {chapter}{\numberline {4}Závěr}{11}{chapter.4}%
\contentsline {section}{\numberline {4.1}Splnění cílů}{11}{section.4.1}%
\contentsline {section}{\numberline {4.2}Jak mě práce bavila}{11}{section.4.2}%
\contentsline {chapter}{\numberline {5}Zdroje}{12}{chapter.5}%
