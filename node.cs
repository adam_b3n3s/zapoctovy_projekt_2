// This file is used to have class with definition of node

using System;
using System.Collections.Generic;
using static System.Console;
using linked_list;
namespace node
{
    public class node<T>
    {
        public T value;
        public node<T> next;
        public node<T> previous;
        public linked_list<T> son;
        public node<T> father;
        public int bin_key; // Bin key is used in fibonacci heap tho control how many sons was detached from node
        public int depth = 1;

        public node(T value) // O(1)
        // This method is used to create instance of node
        {
            this.value = value;
            this.bin_key = 0;
        }
    }
}
