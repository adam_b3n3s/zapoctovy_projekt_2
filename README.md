# Fibonacciho halda

## Jazyk projektu

- Projekt je programován v C#.
- Dokumentace projektu je napsaná v Latexu.

## Co projekt obsahuje

- Vlastnoručne naimplementovaný linked list + Fibonacciho haldu, ta může být využitá k rychlému získávání minima.

## Dokumentace

Dokumentace obsahuje především analýzu Fibinacciho haldy.
