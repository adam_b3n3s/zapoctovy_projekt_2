// This file is having the methods and definition of fibonacci heap

using System;
using System.Collections.Generic;
using static System.Console;
using node;
using linked_list;
namespace fibonacci_heap
{
    public class fibonacci<T> where T : struct
    {
        public int number_of_elements;
        public int max_depth;
        public node<T> minimum;
        public linked_list<T> fibonacci_heap_main;

        // Fibonacci heap is able to make instances two ways
        // 1) If you want to put in arguments as Array you can
        // 2) If you want to put in arguments as List you can
        public fibonacci(params T[] args) // O(n)
        {
            // Create new linked list with arguments
            fibonacci_heap_main = new linked_list<T>(args);
            // Set minimum to head
            this.minimum = fibonacci_heap_main.head;
            node<T> q = this.minimum;
            this.number_of_elements = fibonacci_heap_main.number_of_elements;
            // Set max depth to depth of the minimum
            this.max_depth = this.minimum.depth;
            for (int i = 0; i < fibonacci_heap_main.number_of_elements; i++)
            {
                // Find minimum and max depth by iterating through the list
                if (this.max_depth < q.depth)
                {
                    this.max_depth = q.depth;
                }
                if (Comparer<T>.Default.Compare(this.minimum.value, q.value) > 0)
                {
                    this.minimum = q;
                }
                q = q.next;
            }
        }

        public fibonacci(List<T> numbers) // O(n)
        {
            // Create new linked list with arguments
            fibonacci_heap_main = new linked_list<T>(numbers);
            // Set minimum to head
            this.minimum = fibonacci_heap_main.head;
            node<T> q = this.minimum;
            this.number_of_elements = fibonacci_heap_main.number_of_elements;
            // Set max depth to depth of the minimum
            this.max_depth = this.minimum.depth;
            for (int i = 0; i < fibonacci_heap_main.number_of_elements; i++)
            {
                // Find minimum and max depth by iterating through the list
                if (this.max_depth < q.depth)
                {
                    this.max_depth = q.depth;
                }
                if (Comparer<T>.Default.Compare(this.minimum.value, q.value) > 0)
                {
                    this.minimum = q;
                }
                q = q.next;
            }
        }


        public void insert(T value) // O(1)
        // This method is used to insert node into the heap
        {
            node<T> node = new node<T>(value);
            this.fibonacci_heap_main.add_last(node);

            if (Comparer<T>.Default.Compare(this.minimum.value, node.value) > 0)
            {
                // If the value is smaller than minimum set it as minimum
                this.minimum = node;
            }
            // Increase number of elements
            this.number_of_elements = this.number_of_elements + 1;
        }
        public void insert(node<T> node) // O(1)
        // This method is used to insert node into the heap
        {
            this.fibonacci_heap_main.add_last(node);

            if (Comparer<T>.Default.Compare(this.minimum.value, node.value) > 0)
            {
                // If the value is smaller than minimum set it as minimum
                this.minimum = node;
            }
            // Increase number of elements
            this.number_of_elements = this.number_of_elements + 1;
        }
        public T min_val() // O(1)
        // This method is used to return minimum value in the heap
        {
            return minimum.value;
        }
        public void merge(fibonacci<T> list2) // O(1)
        // This method is used to merge two fibonacci heaps
        // It is done by merging two linked lists and then finding minimum
        {
            this.fibonacci_heap_main.merge(list2.fibonacci_heap_main);

            // Find minimum and max depth by comparing minimums and max depths of both lists
            if (Comparer<T>.Default.Compare(this.minimum.value, list2.minimum.value) > 0)
            {
                this.minimum = list2.minimum;
            }
            if (this.max_depth < list2.max_depth)
            {
                this.max_depth = list2.max_depth;
            }
            // Increase number of elements to the sum of both
            this.number_of_elements = this.number_of_elements + list2.number_of_elements;
        }

        void consolidate()
        // This method is used to consolidate the heap
        // Merge nodes with same depth
        // It is used in extract_min (because it leaves the time complexity of extract_min to amortized O(log(n))
        {
            // It is done by creating array of nodes and then iterating through the list
            List<node<T>> array = new List<node<T>>();

            node<T> akt = this.fibonacci_heap_main.head;

            void doo()
            // It is here just to make the code more readable
            {
                while (array.Count <= akt.depth)
                {
                    // If the array is too small we need to make it bigger
                    array.Add(null);
                }

                if (array[akt.depth] == null)
                {
                    // If there is no node with same depth in the array we just put it there
                    array[akt.depth] = akt;
                    akt = akt.next;
                }
                else
                {
                    // Otherwise we need to merge them
                    node<T> x = array[akt.depth];
                    array[akt.depth] = null;
                    // We need to check which one is smaller and then put the other one as son of the smaller one
                    if (Comparer<T>.Default.Compare(x.value, akt.value) > 0)
                    {
                        if (akt.son == null) { akt.son = new linked_list<T>(); } // check if it is null and if so create it
                        node<T> helper = akt.next; // We need to save the next node because we are going to delete akt from the list
                        this.fibonacci_heap_main.delete_node(x); // Delete x from the list
                        akt.son.add_last(x); // Set x as son of akt
                        x.father = akt; // Set father of x to akt
                        akt.depth += 1; // Increase depth of the node
                        fibonacci_heap_main.delete_node(akt); // Delete akt from the list
                        fibonacci_heap_main.add_last(akt); // Add akt to the end of the list
                        akt = helper; // Set akt to the next node
                    }
                    else
                    {
                        if (x.son == null) { x.son = new linked_list<T>(); } // check if it is null and if so create it
                        node<T> helper = akt.next; // We need to save the next node because we are going to delete akt from the list
                        this.fibonacci_heap_main.delete_node(akt); // Delete akt from the list
                        x.son.add_last(akt); // Set akt as son of x
                        akt.father = x; // Set father of akt to x
                        x.depth += 1; // Increase depth of the node
                        fibonacci_heap_main.delete_node(x); // Delete x from the list
                        fibonacci_heap_main.add_last(x); // Add x to the end of the list
                        akt = helper; // Set akt to the next node
                    }
                }
            }


            while (akt != this.fibonacci_heap_main.tail)
            {
                // Iterate through the list
                doo();
            }
            doo();
        }
        public T extract_min() // Amortized O(log(n))
        // This method is used to extract minimum from the heap
        {
            if (this.number_of_elements == 1)
            {
                // If there is only one element we just delete it
                T minum = this.minimum.value;
                this.fibonacci_heap_main.delete_node(this.minimum);
                this.minimum = null;
                this.number_of_elements = this.number_of_elements - 1;
                return minum;
            }
            if (this.number_of_elements == 0)
            {
                // If there is no element we return default value
                return default(T);
            }

            T minimum = this.minimum.value; // Save the minimum value to retun it later
            if (this.minimum.son != null)
            {
                // If the minimum has sons we need to merge them with the main list
                linked_list<T> min_son = this.minimum.son;
                this.fibonacci_heap_main.merge(min_son);
            }
            this.fibonacci_heap_main.delete_node(this.minimum); // Delete the minimum from the list
            this.minimum = this.fibonacci_heap_main.head; // Set the minimum to the head of the list
            this.number_of_elements = this.number_of_elements - 1; // Decrease number of elements

            consolidate(); // Consolidate the heap

            // Find new minimum by iterating through the main list
            node<T> akt = this.fibonacci_heap_main.head.next;

            while (akt != this.fibonacci_heap_main.head)
            {
                if (Comparer<T>.Default.Compare(this.minimum.value, akt.value) > 0)
                {
                    this.minimum = akt;
                }
                akt = akt.next;
            }
            if (Comparer<T>.Default.Compare(this.minimum.value, this.fibonacci_heap_main.head.value) > 0)
            {
                this.minimum = this.fibonacci_heap_main.head;
            }

            return minimum;
        }

        public void delete(node<T> node) // Amortized O(log(n))
        // This method is used to delete node from the heap
        {
            // This method is done by decreasing the value of the node to the minimum -1 and then extracting the minimum
            T subtract_one_from_value(T value)
            // This method is used to subtract one from the value
            {
                // Dynamic is used because we dont know what type of value we are going to get
                dynamic dynamic_value = value;
                dynamic subtracted_value = dynamic_value - 1;
                return subtracted_value;
            }
            this.decrease(node, subtract_one_from_value(this.minimum.value));
            this.extract_min(); // Then we extract the minimum
        }

        void cut(node<T> node)
        // This method is used to cut node from the heap
        // That means that we detach it from the heap and put it to the main list
        // It is used in decrease
        {
            node.father.bin_key += 1; // Increase bin key of the father
            if (node.father == null)
            {
                return;
            }
            if (node.father.son.number_of_elements == 1)
            {
                // If father has only one son we need to delete it
                node.father.son = null;
            }
            else
            {
                // Otherwise we need to delete it from the list
                node.father.son.delete_node(node);
            }
            this.fibonacci_heap_main.add_last(node);
            if (Comparer<T>.Default.Compare(this.minimum.value, node.value) > 0)
            {
                // If the value is smaller than minimum set it as minimum (This is done because we use it in decrease)
                this.minimum = node;
            }
            if (node.father.bin_key == 2)
            {
                // If the bin key is 2 we need to cut the father
                node.father.bin_key = 0;
                cut(node.father);
            }
            node.father = null; // It has no father so we set it to null
        }

        public void decrease(node<T> node, T value) // Amortized O(1)
        // This method is used to decrease value of the node
        // I wanted to implemented as change value (That would mean you can also increase the value but it has not much sense in fibonacci heap)
        {
            if (Comparer<T>.Default.Compare(node.value, value) <= 0)
            {
                // If the value is bigger than the value we want to change it to we just return
                return;
            }
            node.value = value;
            if (Comparer<T>.Default.Compare(this.minimum.value, node.value) > 0)
            {
                // If the value is smaller than minimum set it as minimum (This is done because we use it in decrease)
                this.minimum = node;
            }
            if (node.father == null)
            {
                // If father does not exists we just return
                return;
            }
            if (Comparer<T>.Default.Compare(node.father.value, value) <= 0)
            {
                // If the value of the father is smaller than the value we want to change it to we just return
                return;
            }
            cut(node); // Otherwise we cut the node
            node<T> akt = this.fibonacci_heap_main.head;
            while (akt != this.fibonacci_heap_main.tail)
            {
                // Iterate through the list and set bin key of all nodes in main list to 0
                akt.bin_key = 0;
                akt = akt.next;
            }
            this.fibonacci_heap_main.tail.bin_key = 0;
        }
        public void print_heap() // O(n)
        // This method is used to print the heap
        {
            // I wanted to print it in a way that it would be readable
            // I wanted to print it in colors
            ForegroundColor = ConsoleColor.Blue; // Set color to blue
            Write("#elements in heap: ");
            ResetColor(); // Reset color to default
            WriteLine(this.number_of_elements);
            ForegroundColor = ConsoleColor.Blue;
            Write("#elements in root: ");
            ResetColor();
            WriteLine(this.fibonacci_heap_main.number_of_elements);
            if (this.number_of_elements == 0)
            {
                // If there is no element in the heap print empty
                WriteLine("empty");
            }
            else
            {
                ForegroundColor = ConsoleColor.Red;
                Write("Root: ");
                ResetColor();
                print(this.fibonacci_heap_main, "");
                void print(linked_list<T> x, string s = "")
                // This method is used to go deeper in the heap
                {
                    ForegroundColor = ConsoleColor.Red;
                    Write(s);
                    ResetColor();
                    WriteLine(x.return_list());
                    node<T> akt = x.head;
                    while (akt != x.tail)
                    {
                        if (akt.son != null)
                        {
                            print(akt.son, s + akt.value + ": ");
                        }
                        akt = akt.next;
                    }
                    if (akt.son != null)
                    {
                        print(akt.son, s + akt.value + ": ");
                    }
                }
            }
        }
        public string return_heap() // O(n)
        // This method is used to return the string that would be printed by print_heap
        {
            // I wanted to return it in a way that it would be readable
            string return_string = "";
            return_string += "#elements in heap: ";
            return_string += this.number_of_elements + "\n";
            return_string += "#elements in root: ";
            return_string += this.fibonacci_heap_main.number_of_elements + "\n";
            if (this.number_of_elements == 0)
            {
                // If there is no element in the heap return empty
                return return_string + "empty\n";
            }
            else
            {
                ForegroundColor = ConsoleColor.Red;
                return_string += "Root: ";
                ResetColor();
                print(this.fibonacci_heap_main, "");
                void print(linked_list<T> x, string s = "")
                // This method is used to go deeper in the heap
                {
                    ForegroundColor = ConsoleColor.Red;
                    return_string += s;
                    ResetColor();
                    return_string += x.return_list() + "\n";
                    node<T> akt = x.head;
                    while (akt != x.tail)
                    {
                        if (akt.son != null)
                        {
                            print(akt.son, s + akt.value + ": ");
                        }
                        akt = akt.next;
                    }
                    if (akt.son != null)
                    {
                        print(akt.son, s + akt.value + ": ");
                    }
                }
                return return_string;
            }
        }
    }
}