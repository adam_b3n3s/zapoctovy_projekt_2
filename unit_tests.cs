// This file is used to test the project

using System;
using System.Collections.Generic;
using static System.Console;
using node;
using linked_list;
using fibonacci_heap;

namespace unit_tests
{
    public class tests<T> where T : struct
    {
        // I test those things by comparing the string that would be printed by them
        public bool test(linked_list<T> input, string answer)
        {
            if (input.return_list() == answer) return true;
            return false;
        }
        public bool test(fibonacci<T> input, String answer)
        {
            if (input.return_heap() == answer) return true;
            return false;
        }
    }
}